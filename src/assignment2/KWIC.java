package assignment2;

import java.util.*;

public class KWIC {

    private List<String> ignoreWords = new ArrayList<>();
    private List<String> keyWords = new ArrayList<>();
    private List<String> result = new ArrayList<>();
    private int flag = 0;

    public List<String> getInput() {
        List<String> lines = new ArrayList<String>();
        Scanner scanner = new Scanner(System.in);
        String nextLine = "";
        while (scanner.hasNextLine()) {
            nextLine = scanner.nextLine();
            if (nextLine.isEmpty()) {
                break;

            }
            if (flag == 0 && !nextLine.equalsIgnoreCase("::")) {
                ignoreWords.add(nextLine.toLowerCase(Locale.ROOT).trim());
            }
            if (nextLine.equalsIgnoreCase("::")) {
                flag = 1;
            }
            if (flag == 1 && !nextLine.equalsIgnoreCase("::")) {
                keyWords.add(nextLine.toLowerCase(Locale.ROOT).trim());
            }
            lines.add(nextLine);
        }
        return lines;
    }


    public void generator() {
        for (int k = 0, keyWordsSize = keyWords.size(); k < keyWordsSize; k++) {
            String keyWord = keyWords.get(k);
            String[] words = keyWord.split(" ");
            for (int i = 0; i < words.length; i++) {

                if (!this.ignoreWords.contains(words[i])) {
                    String currentSeq = "";
                    currentSeq += (words[i].toUpperCase(Locale.ROOT) + k + keyWord.indexOf(words[i]) + "0");
                    for (int j = 0; j < words.length; j++) {
                        if (i == j) {
                            currentSeq += words[i].toUpperCase(Locale.ROOT);
                        } else {
                            currentSeq += words[j];
                        }
                        currentSeq += " ";
                    }
                    result.add(currentSeq.trim());

                }
            }
        }
        Collections.sort(result);
    }

    public void output() {
        Collections.sort(this.result);
        for (String s : this.result) {
            System.out.println(s.substring(s.lastIndexOf("0") + 1, s.length()));
        }
    }


}
