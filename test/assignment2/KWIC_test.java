package assignment2;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class KWIC_test {
    KWIC kwicGenerator;

    @BeforeEach
    public void setUp() {
        kwicGenerator = new KWIC();
    }

    @Test
    public void testInput() {

        final InputStream oldIn = System.in;
        FileInputStream in = null;
        try {
            in = new FileInputStream(new File("test/assignment2/imput.txt"));
            System.setIn(in);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        List<String> lines = kwicGenerator.getInput();
        assertEquals(lines.size(), createInputLines().size());
        assertEquals(lines.get(0), createInputLines().get(0));
        System.setIn(oldIn);
    }

    @Test
    public void testOutputWith5Sentence() {
        final InputStream oldIn = System.in;
        FileInputStream in = null;
        final PrintStream oldOut = System.out;
        final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        try {
            in = new FileInputStream(new File("test/assignment2/imput.txt"));
            System.setIn(in);
            System.setOut(new PrintStream(outContent));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        List<String> lines = kwicGenerator.getInput();
        kwicGenerator.generator();
        kwicGenerator.output();
        assertEquals("""
                a portrait of the ARTIST as a young man\r
                the ASCENT of man\r
                a man is a man but BUBBLESORT is a dog\r
                DESCENT of man\r
                a man is a man but bubblesort is a DOG\r
                descent of MAN\r
                the ascent of MAN\r
                the old MAN and the sea\r
                a portrait of the artist as a young MAN\r
                a MAN is a man but bubblesort is a dog\r
                a man is a MAN but bubblesort is a dog\r
                the OLD man and the sea\r
                a PORTRAIT of the artist as a young man\r
                the old man and the SEA\r
                a portrait of the artist as a YOUNG man""", outContent.toString().trim());
        System.setIn(oldIn);

        System.setOut(oldOut);


    }

    private List<String> createInputLines() {
        List<String> lines = new ArrayList<String>();
        lines.add("is");
        lines.add("the");
        lines.add("of");
        lines.add("and");
        lines.add("as");
        lines.add("a");
        lines.add("but");
        lines.add("::");
        lines.add("Descent of Man");
        lines.add("The Ascent of Man");
        lines.add("The Old Man and The Sea");
        lines.add("A Portrait of The Artist As a Young Man");
        lines.add("A Man is a Man but Bubblesort IS A DOG");
        return lines;
    }


}
